---
title: Maths, AI and Intuition
date: 2022-05-19
edate: 2022-06-30 18:00:00
location: Online
---
![](/images/maths-ai-intuition.jpeg)

The following event is hosted by AMSI but may still be interesting for students part of MASSOC:

Join Professor Geordie Williamson (The University of Sydney) on Thursday 30 June AEST as he explores how machine learning can help with the intuitive aspects of mathematical research.

This lecture is suitable for mathematics and AI enthusiasts from age 15 and up. Attend this FREE EVENT online or in-person at The University of Queensland. 

Register [here](https://ws.amsi.org.au/public-lecture/).
