--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}

import Data.Time.Clock (UTCTime)
import Data.Time.Format (formatTime, parseTimeM)
import Data.Time.Locale.Compat (defaultTimeLocale)
import Hakyll
import System.FilePath.Posix ( takeDirectory
                             , takeBaseName
                             , (</>)
                             , splitFileName
                             )
import Data.List (isInfixOf)
import Control.Applicative

--------------------------------------------------------------------------------

main :: IO ()
main = hakyll $ do
  match "images/*" $ do
    route idRoute
    compile copyFileCompiler

  match "fonts/**" $ do
    route idRoute
    compile copyFileCompiler

  match "css/*" $ do
    route idRoute
    compile compressCssCompiler

  match "posts/*" $ do
    route cleanRoute
    compile $
      pandocCompiler
        >>= loadAndApplyTemplate "templates/post.html" dateCtx
        >>= loadAndApplyTemplate "templates/default.html" dateCtx
        >>= relativizeUrls
        >>= removeIndexHtml

  create ["archive.html"] $ do
    route cleanRoute
    compile $ do
      posts <- recentFirst =<< loadAll "posts/*"
      let archiveCtx =
            listField "posts" dateCtx (return posts)
            `mappend` constField "title" "Archives"
            `mappend` defaultContext

      makeItem ""
        >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
        >>= loadAndApplyTemplate "templates/default.html" archiveCtx
        >>= relativizeUrls
        >>= removeIndexHtml

  match "events/*" $ do
    route cleanRoute
    compile $
      pandocCompiler
        >>= loadAndApplyTemplate "templates/event.html" dateCtx
        >>= loadAndApplyTemplate "templates/default.html" dateCtx
        >>= relativizeUrls
        >>= removeIndexHtml

  create ["upcoming.html"] $ do
    route cleanRoute
    compile $ do
      events <- recentFirst =<< loadAll "events/*"
      let upcomingCtx =
            listField "events" dateCtx (return events)
            `mappend` constField "title" "Upcoming Events"
            `mappend` defaultContext

      makeItem ""
        >>= loadAndApplyTemplate "templates/upcoming.html" upcomingCtx
        >>= loadAndApplyTemplate "templates/default.html" upcomingCtx
        >>= relativizeUrls
        >>= removeIndexHtml

  match "minutes/*" $ do
    route cleanRoute
    compile $
      pandocCompiler
        >>= loadAndApplyTemplate "templates/minutes.html" dateCtx
        >>= loadAndApplyTemplate "templates/default.html" dateCtx
        >>= relativizeUrls
        >>= removeIndexHtml

  create ["about.html"] $ do
    route cleanRoute
    compile $ do
      minutes <- recentFirst =<< loadAll "minutes/*"
      let aboutCtx =
            listField "minutes" dateCtx (return minutes)
            `mappend` constField "title" "About"
            `mappend` defaultContext

      makeItem ""
        >>= loadAndApplyTemplate "templates/about.html" aboutCtx
        >>= loadAndApplyTemplate "templates/default.html" aboutCtx
        >>= relativizeUrls
        >>= removeIndexHtml

  match "index.html" $ do
    route idRoute
    compile $ do
      posts <- recentFirst =<< loadAll "posts/*"
      events <- recentFirst =<< loadAll "events/*"
      let indexCtx =
            listField "posts" dateCtx (return posts)
            `mappend` listField "events" dateCtx (return events)
            `mappend` defaultContext

      getResourceBody
        >>= applyAsTemplate indexCtx
        >>= loadAndApplyTemplate "templates/default.html" indexCtx
        >>= relativizeUrls
        >>= removeIndexHtml

  match "templates/*" $ compile templateBodyCompiler

--------------------------------------------------------------------------------

dateCtx :: Context String
dateCtx =
  dateField "date" "%e %B, %Y"
  `mappend` formatDateFieldValue "edate" "%l:%M %p, %e %B, %Y"
  `mappend` defaultContext

cleanRoute :: Routes
cleanRoute = customRoute createIndexRoute
  where
    createIndexRoute ident = takeDirectory p </> takeBaseName p </> "index.html"
      where p = toFilePath ident

removeIndexHtml :: Item String -> Compiler (Item String)
removeIndexHtml item = return $ fmap (withUrls removeIndexStr) item

removeIndexStr :: String -> String
removeIndexStr url = case splitFileName url of
    (dir, "index.html") | isLocal dir -> dir
                        | otherwise -> url
    _ -> url
    where
        isLocal :: String -> Bool
        isLocal uri = not ("://" `isInfixOf` uri)

formatDateFieldValue :: String -> String -> Context a
formatDateFieldValue name fmt = Context $ \k _ i ->
  if k == name
  then (do value <- getMetadataField (itemIdentifier i) k
           maybe empty (\v -> do
                           let mSDate = parseAndFormat fmt v
                           case mSDate of
                             (Just sDate) -> (return . StringField) sDate
                             Nothing  -> empty
                       ) value
       )
  else empty
  where
    parseAndFormat :: String -> String -> Maybe String
    parseAndFormat fmt' v' = do
      let timeV = parseTimeM True defaultTimeLocale "%Y-%m-%e %H:%M:%S" v' :: Maybe UTCTime
      case timeV of
        (Just t) -> Just $ formatTime defaultTimeLocale fmt' t
        Nothing   -> Nothing
