---
title: Announcing Our End of Year Dinner
author: Lauren Allen
---

![](/images/2021_end_of_year.jpg)

The UOW MASSOC welcome all new and graduating students, as well as academic staff to our end of year dinner! This event will be hosted at RASHAYS North Wollongong starting at 6pm December 2nd. It has been a tough year for all and a year our club has not been able to secure much funding or complete much fundraising within. Therefore, we respectfully note that all patrons will needs to purchase their own meals and beverages, as they please, per the restaurants pricing. Please see the venue’s menu [here](https://www.rashays.com/dine-in/).
